// Utility function to create a mutation observer
function createAttributeObserver(callback) {
  const MutationObserver =
    window.MutationObserver ||
    window.WebKitMutationObserver ||
    window.MozMutationObserver;
  return new MutationObserver((mutations) => {
    mutations.forEach((mutation) =>
      callback(mutation.target, mutation.attributeName)
    );
  });
}

// Function to handle details item changes
function handleDetailsChange(changedItem, attribute) {
  if (attribute === "open") {
    const container = changedItem.closest(".accordion-container");
    const allItems = container.querySelectorAll(".details-item");
    const content = changedItem.querySelector(".details-content");

    if (changedItem.hasAttribute("open")) {
      // Close other open items
      allItems.forEach((item) => {
        if (item !== changedItem && item.hasAttribute("open")) {
          const itemContent = item.querySelector(".details-content");
          itemContent.style.opacity = "0";
          itemContent.style.maxHeight = "0";
          item.removeAttribute("open");
        }
      });

      // Open the clicked item
      content.style.setProperty(
        "--details-height",
        `${content.scrollHeight}px`
      );
      requestAnimationFrame(() => {
        content.style.opacity = "1";
        content.style.maxHeight = `${content.scrollHeight}px`;
      });
    } else {
      // Item is being closed
      content.style.opacity = "0";
      content.style.maxHeight = "0";
    }
  }
}

// Function to prevent default action for open details
function preventDefaultForOpenDetails(event) {
  const tab = event.target.closest(".details-tab");
  if (!tab) return; // Exit if the click wasn't on a tab

  const detailsElement = tab.closest(".details-item");
  if (detailsElement && detailsElement.hasAttribute("open")) {
    event.preventDefault();
  }
}

// Function to handle keyboard events for tabs
function handleTabKeyboard(event) {
  const tab = event.target.closest(".details-tab");
  if (!tab) return; // Exit if the key event wasn't on a tab

  if (event.key === "Enter" || event.key === " ") {
    preventDefaultForOpenDetails(event);
  }
}

function updateOpenItemsHeight() {
  const openItems = document.querySelectorAll(
    ".details-item[open] .details-content"
  );
  openItems.forEach((content) => {
    content.style.maxHeight = "none"; // Temporarily remove max-height to get true height
    const height = content.scrollHeight;
    requestAnimationFrame(() => {
      content.style.maxHeight = `${height}px`;
    });
  });
}

// Main function to set up the behavior
function setupAccordionBehavior() {
  const accordionContainer = document.querySelector(".accordion-container");
  if (!accordionContainer) {
    console.warn("Accordion container not found");
    return;
  }

  const detailsItems = accordionContainer.querySelectorAll(".details-item");

  // Set up attribute change observation
  const observer = createAttributeObserver(handleDetailsChange);
  detailsItems.forEach((item) => observer.observe(item, { attributes: true }));

  // Set up event delegation for tabs on the accordion container
  accordionContainer.addEventListener("click", preventDefaultForOpenDetails);
  accordionContainer.addEventListener("keydown", handleTabKeyboard);

  // Add resize event listener
  window.addEventListener("resize", updateOpenItemsHeight);
}

// Initialize the behavior when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", setupAccordionBehavior);
